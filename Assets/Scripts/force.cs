using System.Collections;
using System.Collections.Generic;
using TMPro.EditorUtilities;
using UnityEngine;

public class force : MonoBehaviour
{
    private Rigidbody2D _rigid;
    public Vector2 _position; 
    public float _accel;
    // Start is called before the first frame update
    void Start()
    {
        _rigid = GetComponent<Rigidbody2D>();    
    }

    // Update is called once per frame
    void Update()
    { 
        if (Mathf.Cos(Time.timeSinceLevelLoad) == 1)
        {
            _rigid.AddForce(_position.normalized * _accel, ForceMode2D.Impulse);
        }
    }
}
