using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loss : MonoBehaviour
{
    private HingeJoint2D _joint;
    // Start is called before the first frame update
    void Start()
    {
        _joint = GetComponent<HingeJoint2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _joint.breakForce = 0;
        }
    }
}
